{
  const {
    html,
  } = Polymer;
  /**
    `<miguelr-dashboard>` Description.

    Example:

    ```html
    <miguelr-dashboard></miguelr-dashboard>
    ```

    ## Styling
    The following custom properties and mixins are available for styling:

    ### Custom Properties
    | Custom Property     | Selector | CSS Property | Value       |
    | ------------------- | -------- | ------------ | ----------- |
    | --cells-fontDefault | :host    | font-family  |  sans-serif |
    ### @apply
    | Mixins    | Selector | Value |
    | --------- | -------- | ----- |
    | --miguelr-dashboard | :host    | {} |

    * @customElement
    * @polymer
    * @extends {Polymer.Element}
    * @demo demo/index.html
  */
  class MiguelrDashboard extends Polymer.Element {

    static get is() {
      return 'miguelr-dashboard';
    }

    static get properties() {
      return {

        academia: {

          type: Object,
          notify: true,
          value: {}

        },

        user:{
          type: String,
          value:''
        }

      };
    }

    static get template() {
      return html `
      <style include="miguelr-dashboard-styles miguelr-dashboard-shared-styles"></style>
      <slot></slot>
      
         

      

      <div>
        <p> Filter <input type="text" placeholder="User" value="{{user::input}}" on-keyup="filtro"></p>
        <cells-mocks-component alumnos="{{academia}}"></cells-mocks-component>
        <iron-ajax auto url="https://api.chucknorris.io/jokes/random" handle-as="json" last-response="{{handleResponse}}"></iron-ajax>


      </div>

      <div>
        [[handleResponse.value]]
      </div>

      <template is="dom-repeat" items="{{academia}}">
          
          <div class="card">
              
            <br>
            <img src="{{item.img}}">
            <div>nombre: <span>[[item.name]]</span></div>
            <div>last name: <span>[[item.last]]</span></div>
            <div>Address <span>[[item.name]]</span></div>
            <div>Hobbie: <span>[[item.hobbies]]</span></div>
            <br>
    
          </div>
    
        </template>
      
      `;
    }
  }

  customElements.define(MiguelrDashboard.is, MiguelrDashboard);
}